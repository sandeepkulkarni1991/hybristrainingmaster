/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Feb 17, 2022, 10:35:13 AM                   ---
 * ----------------------------------------------------------------
 */
package org.training.core.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedTrainingcoreConstants
{
	public static final String EXTENSIONNAME = "trainingcore";
	public static class TC
	{
		public static final String MYCUSTOMERREVIEW = "MyCustomerReview".intern();
		public static final String WORKINGMODELENUM = "WorkingModelEnum".intern();
	}
	public static class Attributes
	{
		public static class Product
		{
			public static final String SAMPLEFIELD = "sampleField".intern();
		}
		public static class ZoneDeliveryMode
		{
			public static final String MODEDELIVERYTIME = "modeDeliveryTime".intern();
			public static final String WORKINGMODEL = "workingModel".intern();
		}
	}
	public static class Enumerations
	{
		public static class ArticleApprovalStatus
		{
			public static final String DECOMMISION = "decommision".intern();
		}
		public static class WorkingModelEnum
		{
			public static final String BACK2BACK = "BACK2BACK".intern();
			public static final String DROPSHIPMENT = "DROPSHIPMENT".intern();
		}
	}
	
	protected GeneratedTrainingcoreConstants()
	{
		// private constructor
	}
	
	
}
