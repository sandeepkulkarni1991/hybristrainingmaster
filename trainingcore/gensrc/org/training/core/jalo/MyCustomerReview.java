/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Feb 17, 2022, 10:35:13 AM                   ---
 * ----------------------------------------------------------------
 */
package org.training.core.jalo;

import de.hybris.platform.directpersistence.annotation.SLDSafe;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.media.Media;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type MyCustomerReview.
 */
@SLDSafe
@SuppressWarnings({"unused","cast"})
public class MyCustomerReview extends GenericItem
{
	/** Qualifier of the <code>MyCustomerReview.myReview</code> attribute **/
	public static final String MYREVIEW = "myReview";
	/** Qualifier of the <code>MyCustomerReview.givenReview</code> attribute **/
	public static final String GIVENREVIEW = "givenReview";
	/** Qualifier of the <code>MyCustomerReview.media</code> attribute **/
	public static final String MEDIA = "media";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(MYREVIEW, AttributeMode.INITIAL);
		tmp.put(GIVENREVIEW, AttributeMode.INITIAL);
		tmp.put(MEDIA, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>MyCustomerReview.givenReview</code> attribute.
	 * @return the givenReview - Example Initial Boolean Field
	 */
	public Boolean isGivenReview(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, "givenReview".intern());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>MyCustomerReview.givenReview</code> attribute.
	 * @return the givenReview - Example Initial Boolean Field
	 */
	public Boolean isGivenReview()
	{
		return isGivenReview( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>MyCustomerReview.givenReview</code> attribute. 
	 * @return the givenReview - Example Initial Boolean Field
	 */
	public boolean isGivenReviewAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isGivenReview( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>MyCustomerReview.givenReview</code> attribute. 
	 * @return the givenReview - Example Initial Boolean Field
	 */
	public boolean isGivenReviewAsPrimitive()
	{
		return isGivenReviewAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>MyCustomerReview.givenReview</code> attribute. 
	 * @param value the givenReview - Example Initial Boolean Field
	 */
	public void setGivenReview(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, "givenReview".intern(),value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>MyCustomerReview.givenReview</code> attribute. 
	 * @param value the givenReview - Example Initial Boolean Field
	 */
	public void setGivenReview(final Boolean value)
	{
		setGivenReview( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>MyCustomerReview.givenReview</code> attribute. 
	 * @param value the givenReview - Example Initial Boolean Field
	 */
	public void setGivenReview(final SessionContext ctx, final boolean value)
	{
		setGivenReview( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>MyCustomerReview.givenReview</code> attribute. 
	 * @param value the givenReview - Example Initial Boolean Field
	 */
	public void setGivenReview(final boolean value)
	{
		setGivenReview( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>MyCustomerReview.media</code> attribute.
	 * @return the media
	 */
	public Media getMedia(final SessionContext ctx)
	{
		return (Media)getProperty( ctx, "media".intern());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>MyCustomerReview.media</code> attribute.
	 * @return the media
	 */
	public Media getMedia()
	{
		return getMedia( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>MyCustomerReview.media</code> attribute. 
	 * @param value the media
	 */
	public void setMedia(final SessionContext ctx, final Media value)
	{
		setProperty(ctx, "media".intern(),value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>MyCustomerReview.media</code> attribute. 
	 * @param value the media
	 */
	public void setMedia(final Media value)
	{
		setMedia( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>MyCustomerReview.myReview</code> attribute.
	 * @return the myReview - My Example Initial String Value
	 */
	public String getMyReview(final SessionContext ctx)
	{
		return (String)getProperty( ctx, "myReview".intern());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>MyCustomerReview.myReview</code> attribute.
	 * @return the myReview - My Example Initial String Value
	 */
	public String getMyReview()
	{
		return getMyReview( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>MyCustomerReview.myReview</code> attribute. 
	 * @param value the myReview - My Example Initial String Value
	 */
	public void setMyReview(final SessionContext ctx, final String value)
	{
		setProperty(ctx, "myReview".intern(),value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>MyCustomerReview.myReview</code> attribute. 
	 * @param value the myReview - My Example Initial String Value
	 */
	public void setMyReview(final String value)
	{
		setMyReview( getSession().getSessionContext(), value );
	}
	
}
