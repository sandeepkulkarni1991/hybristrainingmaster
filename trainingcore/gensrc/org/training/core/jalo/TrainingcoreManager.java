/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Feb 17, 2022, 10:35:13 AM                   ---
 * ----------------------------------------------------------------
 */
package org.training.core.jalo;

import de.hybris.platform.deliveryzone.jalo.ZoneDeliveryMode;
import de.hybris.platform.directpersistence.annotation.SLDSafe;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.JaloSystemException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.extension.Extension;
import de.hybris.platform.jalo.extension.ExtensionManager;
import de.hybris.platform.jalo.order.delivery.DeliveryMode;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.JaloGenericCreationException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.training.core.constants.TrainingcoreConstants;
import org.training.core.jalo.MyCustomerReview;

/**
 * Generated class for type <code>TrainingcoreManager</code>.
 */
@SuppressWarnings({"unused","cast"})
@SLDSafe
public class TrainingcoreManager extends Extension
{
	protected static final Map<String, Map<String, AttributeMode>> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, Map<String, AttributeMode>> ttmp = new HashMap();
		Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put("sampleField", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.jalo.product.Product", Collections.unmodifiableMap(tmp));
		tmp = new HashMap<String, AttributeMode>();
		tmp.put("modeDeliveryTime", AttributeMode.INITIAL);
		tmp.put("workingModel", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.deliveryzone.jalo.ZoneDeliveryMode", Collections.unmodifiableMap(tmp));
		DEFAULT_INITIAL_ATTRIBUTES = ttmp;
	}
	@Override
	public Map<String, AttributeMode> getDefaultAttributeModes(final Class<? extends Item> itemClass)
	{
		Map<String, AttributeMode> ret = new HashMap<>();
		final Map<String, AttributeMode> attr = DEFAULT_INITIAL_ATTRIBUTES.get(itemClass.getName());
		if (attr != null)
		{
			ret.putAll(attr);
		}
		return ret;
	}
	
	public MyCustomerReview createMyCustomerReview(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType("MyCustomerReview");
			return (MyCustomerReview)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating MyCustomerReview : "+e.getMessage(), 0 );
		}
	}
	
	public MyCustomerReview createMyCustomerReview(final Map attributeValues)
	{
		return createMyCustomerReview( getSession().getSessionContext(), attributeValues );
	}
	
	public static final TrainingcoreManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (TrainingcoreManager) em.getExtension(TrainingcoreConstants.EXTENSIONNAME);
	}
	
	@Override
	public String getName()
	{
		return TrainingcoreConstants.EXTENSIONNAME;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ZoneDeliveryMode.modeDeliveryTime</code> attribute.
	 * @return the modeDeliveryTime
	 */
	public Integer getModeDeliveryTime(final SessionContext ctx, final ZoneDeliveryMode item)
	{
		return (Integer)item.getProperty( ctx, TrainingcoreConstants.Attributes.ZoneDeliveryMode.MODEDELIVERYTIME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ZoneDeliveryMode.modeDeliveryTime</code> attribute.
	 * @return the modeDeliveryTime
	 */
	public Integer getModeDeliveryTime(final ZoneDeliveryMode item)
	{
		return getModeDeliveryTime( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ZoneDeliveryMode.modeDeliveryTime</code> attribute. 
	 * @return the modeDeliveryTime
	 */
	public int getModeDeliveryTimeAsPrimitive(final SessionContext ctx, final ZoneDeliveryMode item)
	{
		Integer value = getModeDeliveryTime( ctx,item );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ZoneDeliveryMode.modeDeliveryTime</code> attribute. 
	 * @return the modeDeliveryTime
	 */
	public int getModeDeliveryTimeAsPrimitive(final ZoneDeliveryMode item)
	{
		return getModeDeliveryTimeAsPrimitive( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ZoneDeliveryMode.modeDeliveryTime</code> attribute. 
	 * @param value the modeDeliveryTime
	 */
	public void setModeDeliveryTime(final SessionContext ctx, final ZoneDeliveryMode item, final Integer value)
	{
		item.setProperty(ctx, TrainingcoreConstants.Attributes.ZoneDeliveryMode.MODEDELIVERYTIME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ZoneDeliveryMode.modeDeliveryTime</code> attribute. 
	 * @param value the modeDeliveryTime
	 */
	public void setModeDeliveryTime(final ZoneDeliveryMode item, final Integer value)
	{
		setModeDeliveryTime( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ZoneDeliveryMode.modeDeliveryTime</code> attribute. 
	 * @param value the modeDeliveryTime
	 */
	public void setModeDeliveryTime(final SessionContext ctx, final ZoneDeliveryMode item, final int value)
	{
		setModeDeliveryTime( ctx, item, Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ZoneDeliveryMode.modeDeliveryTime</code> attribute. 
	 * @param value the modeDeliveryTime
	 */
	public void setModeDeliveryTime(final ZoneDeliveryMode item, final int value)
	{
		setModeDeliveryTime( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.sampleField</code> attribute.
	 * @return the sampleField - List of genders that the ApparelProduct is designed for
	 */
	public String getSampleField(final SessionContext ctx, final Product item)
	{
		return (String)item.getProperty( ctx, TrainingcoreConstants.Attributes.Product.SAMPLEFIELD);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.sampleField</code> attribute.
	 * @return the sampleField - List of genders that the ApparelProduct is designed for
	 */
	public String getSampleField(final Product item)
	{
		return getSampleField( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.sampleField</code> attribute. 
	 * @param value the sampleField - List of genders that the ApparelProduct is designed for
	 */
	public void setSampleField(final SessionContext ctx, final Product item, final String value)
	{
		item.setProperty(ctx, TrainingcoreConstants.Attributes.Product.SAMPLEFIELD,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.sampleField</code> attribute. 
	 * @param value the sampleField - List of genders that the ApparelProduct is designed for
	 */
	public void setSampleField(final Product item, final String value)
	{
		setSampleField( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ZoneDeliveryMode.workingModel</code> attribute.
	 * @return the workingModel
	 */
	public EnumerationValue getWorkingModel(final SessionContext ctx, final ZoneDeliveryMode item)
	{
		return (EnumerationValue)item.getProperty( ctx, TrainingcoreConstants.Attributes.ZoneDeliveryMode.WORKINGMODEL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ZoneDeliveryMode.workingModel</code> attribute.
	 * @return the workingModel
	 */
	public EnumerationValue getWorkingModel(final ZoneDeliveryMode item)
	{
		return getWorkingModel( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ZoneDeliveryMode.workingModel</code> attribute. 
	 * @param value the workingModel
	 */
	public void setWorkingModel(final SessionContext ctx, final ZoneDeliveryMode item, final EnumerationValue value)
	{
		item.setProperty(ctx, TrainingcoreConstants.Attributes.ZoneDeliveryMode.WORKINGMODEL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ZoneDeliveryMode.workingModel</code> attribute. 
	 * @param value the workingModel
	 */
	public void setWorkingModel(final ZoneDeliveryMode item, final EnumerationValue value)
	{
		setWorkingModel( getSession().getSessionContext(), item, value );
	}
	
}
