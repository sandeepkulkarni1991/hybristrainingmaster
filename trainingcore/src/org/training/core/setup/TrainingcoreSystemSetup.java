/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.training.core.setup;

import static org.training.core.constants.TrainingcoreConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import org.training.core.constants.TrainingcoreConstants;
import org.training.core.service.TrainingcoreService;


@SystemSetup(extension = TrainingcoreConstants.EXTENSIONNAME)
public class TrainingcoreSystemSetup
{
	private final TrainingcoreService trainingcoreService;

	public TrainingcoreSystemSetup(final TrainingcoreService trainingcoreService)
	{
		this.trainingcoreService = trainingcoreService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		trainingcoreService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return TrainingcoreSystemSetup.class.getResourceAsStream("/trainingcore/sap-hybris-platform.png");
	}
}
