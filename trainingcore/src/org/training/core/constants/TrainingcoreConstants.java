/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.training.core.constants;

/**
 * Global class for all Trainingcore constants. You can add global constants for your extension into this class.
 */
public final class TrainingcoreConstants extends GeneratedTrainingcoreConstants
{
	public static final String EXTENSIONNAME = "trainingcore";

	private TrainingcoreConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "trainingcorePlatformLogo";
}
