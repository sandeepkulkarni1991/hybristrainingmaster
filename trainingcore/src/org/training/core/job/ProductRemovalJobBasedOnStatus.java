/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.training.core.job;


import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.exceptions.ModelRemovalException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.tx.Transaction;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;
import org.training.core.dao.TrainingProductDao;


public class ProductRemovalJobBasedOnStatus extends AbstractJobPerformable<CronJobModel>
{
	//public static final String SITE_UID = "apparel-uk";
	/*
	 * private CustomProductsDAO customProductsDao;
	 */

	private ModelService modelService;

	@Resource(name = "trainingProductDao")
	private TrainingProductDao trainingProductDao;
	/*
	 * private IndexerService indexerService; private FacetSearchConfigService facetSearchConfigService; private
	 * BaseSiteService baseSiteService;
	 */
	private final static Logger LOG = Logger.getLogger(ProductRemovalJobBasedOnStatus.class.getName());

	@Override
	public PerformResult perform(final CronJobModel cronJobModel)
	{

		//		final List<ProductModel> productModelListToBeDeleted = new ArrayList<>();
		final List<ProductModel> productModelList = trainingProductDao.findProductsByStatusCode("decommision");
		LOG.debug("Products older than specified days size:" + productModelList.size());

		if (!CollectionUtils.isEmpty(productModelList))
		{
			Transaction tx = null;
			try
			{
				tx = Transaction.current();
				tx.begin();
				getModelService().removeAll(productModelList);
				tx.commit();
			}
			catch (final ModelRemovalException e)
			{
				if (null != tx)
				{
					tx.rollback();
				}
				LOG.error("Could not remove the product list -->" + e);
			}
		}
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	@Override
	public void setModelService(final ModelService modelService)
	{

		this.modelService = modelService;
	}

}