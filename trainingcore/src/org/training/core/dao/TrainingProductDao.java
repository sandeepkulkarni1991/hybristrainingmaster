/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.training.core.dao;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.daos.ProductDao;

import java.util.List;


/**
 *
 */
public interface TrainingProductDao extends ProductDao
{
	List<ProductModel> findProductsByStatusCode(final String code);
}
