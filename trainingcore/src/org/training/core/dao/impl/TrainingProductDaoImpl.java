/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.training.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.daos.impl.DefaultProductDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.training.core.dao.TrainingProductDao;


/**
 *
 */
public class TrainingProductDaoImpl extends DefaultProductDao implements TrainingProductDao
{

	static final String FIND_ALL_PRODUCT_BY_VENDOR = "SELECT {p.PK} from {Product as p} where  ({p.approvalStatus}=({{ select {s.pk} from {ArticleApprovalStatus as s} where {s.code}=?code}}))";

	/**
	 *
	 */
	public TrainingProductDaoImpl(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<ProductModel> findProductsByStatusCode(final String code)
	{
		validateParameterNotNull(code, "Product code not be null!");
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_ALL_PRODUCT_BY_VENDOR);
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("code", "decommision");
		query.addQueryParameters(params);
		return getFlexibleSearchService().<ProductModel> search(query).getResult();
	}

}
