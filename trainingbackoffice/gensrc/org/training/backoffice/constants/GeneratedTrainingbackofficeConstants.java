/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Feb 15, 2022, 9:42:40 AM                    ---
 * ----------------------------------------------------------------
 */
package org.training.backoffice.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedTrainingbackofficeConstants
{
	public static final String EXTENSIONNAME = "trainingbackoffice";
	
	protected GeneratedTrainingbackofficeConstants()
	{
		// private constructor
	}
	
	
}
